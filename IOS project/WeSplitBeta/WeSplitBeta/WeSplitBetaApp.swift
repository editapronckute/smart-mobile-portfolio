//
//  WeSplitBetaApp.swift
//  WeSplitBeta
//
//  Created by Edita on 23/02/2021.
//

import SwiftUI

@main
struct WeSplitBetaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
