//
//  AnimationsApp.swift
//  Animations
//
//  Created by Edita on 03/03/2021.
//

import SwiftUI

@main
struct AnimationsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
