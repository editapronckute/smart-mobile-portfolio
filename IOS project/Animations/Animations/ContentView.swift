//
//  ContentView.swift
//  Animations
//
//  Created by Edita on 03/03/2021.
//

import SwiftUI

struct ContentView: View {
    @State private var animationAmount = 0.0
    var body: some View {
        ZStack{
            LinearGradient(gradient: Gradient(colors: [.gray, .green]), startPoint: .topLeading, endPoint: .bottomTrailing)
            .ignoresSafeArea()
        Button("Flip a coin") {
            withAnimation(.interpolatingSpring(stiffness: 5, damping: 1)){
            self.animationAmount += 360
            }
        }
        .padding(50)
        .background(Color.orange)
        .foregroundColor(.yellow)
        .clipShape(Circle())
        .rotation3DEffect(.degrees(animationAmount), axis: (x: 0, y: 1, z: 0))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
