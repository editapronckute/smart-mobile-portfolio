//
//  WeekOverviewView.swift
//  BraveBee
//
//  Created by Ivona on 14/03/2021.
//

import SwiftUI

struct WeekOverviewView: View {
    var body: some View {
        Color.black
                .edgesIgnoringSafeArea(.all) // Ignore just for the color
                .overlay(
                    VStack{
                        Text("Week Overview").bold().foregroundColor(.yellow).padding().frame(width: 300, alignment: .leading).font(.largeTitle)
                        Text("Acrophobia").frame(width: 300, alignment: .leading).foregroundColor(.gray)
                        NavigationLink(destination: ChallengeView()){
                            Text("Week 1").padding()
                                .frame(width: 200, height: 70, alignment: .center)
                                .foregroundColor(.yellow)
                                .background(Color("BeeGray"))
                                .clipShape(RoundedRectangle(cornerRadius: 15, style: .continuous))
                        }
                        NavigationLink(destination: ChallengeView()){
                            Text("Week 2").padding()
                                .frame(width: 200, height: 70, alignment: .center)
                                .foregroundColor(.yellow)
                                .background(Color("BeeGray"))
                                .clipShape(RoundedRectangle(cornerRadius: 15, style: .continuous))
                        }
                        NavigationLink(destination: ChallengeView()){
                            Text("Week 3").padding()
                                .frame(width: 200, height: 70, alignment: .center)
                                .foregroundColor(.yellow)
                                .background(Color("BeeGray"))
                                .clipShape(RoundedRectangle(cornerRadius: 15, style: .continuous))
                        }
                        NavigationLink(destination: ChallengeView()){
                            Text("Week 4").padding()
                                .frame(width: 200, height: 70, alignment: .center)
                                .foregroundColor(.yellow)
                                .background(Color("BeeGray"))
                                .clipShape(RoundedRectangle(cornerRadius: 15, style: .continuous))
                        }
                        NavigationLink(destination: ChallengeView()){
                            Text("Week 5").padding()
                                .frame(width: 200, height: 70, alignment: .center)
                                .foregroundColor(.yellow)
                                .background(Color("BeeGray"))
                                .clipShape(RoundedRectangle(cornerRadius: 15, style: .continuous))
                        }
                    }
//                    .padding()
//                    .cornerRadius(10.0)
                    .frame(width: 359, height: 700, alignment: .top)
                    .navigationBarHidden(false)
                            .toolbar {
                                ToolbarItemGroup(placement: .bottomBar) {
                                    HStack{
                                    NavigationLink(
                                        destination: FearListView())
                                         {Image(systemName: "list.dash")
                                    }
                                        Spacer()
                                        NavigationLink(
                                            destination: SettingsView())
                                             {
                                            Image(systemName: "gear")
                                    }
                                    }.padding().frame(width: 220)
                                }
                            }.accentColor(.gray)
                    
                )
    }
}

struct WeekOverviewView_Previews: PreviewProvider {
    static var previews: some View {
        WeekOverviewView()
    }
}
