//
//  BraveBeeApp.swift
//  BraveBee
//
//  Created by Edita on 09/03/2021.
//

import SwiftUI

@main
struct BraveBeeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
