//
//  FearSearchView.swift
//  BraveBee
//
//  Created by Edita on 10/03/2021.
//

import SwiftUI

struct FearSearchView: View {
        let array = ["Acrophobia", "Arachnophobia", "Claustrophobia", "Philofobia", "Scopophobia"]
        @State private var searchText = ""
        @State private var showCancelButton: Bool = false

        var body: some View {
            Color.black
                .ignoresSafeArea(.all) // Ignore just for the color
                    .overlay(
                VStack {
                    Text("Choose a fear").foregroundColor(.yellow).padding().frame(width: 380, alignment: .leading).font(.largeTitle)
                    // Search view
                    HStack {
                        HStack {
                            Image(systemName: "magnifyingglass")
                            TextField("search", text: $searchText, onEditingChanged: { isEditing in
                                self.showCancelButton = true
                            }, onCommit: {
                                print("onCommit")
                            }).foregroundColor(.yellow)

                            Button(action: {
                                self.searchText = ""
                            }) {
                                Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                            }
                        }
                        .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
                        .foregroundColor(.yellow)
                        .background(Color(.secondarySystemBackground))
                        .cornerRadius(10.0)

                        if showCancelButton  {
                            Button("Cancel") {
                                    UIApplication.shared.EndEditing(true) // this must be placed before the other commands here
                                    self.searchText = ""
                                    self.showCancelButton = false
                            }
                            .foregroundColor(Color(.yellow))
                        }
                    }
                    .padding(.horizontal)
                    .navigationBarHidden(showCancelButton).foregroundColor(.yellow)
                    Text("Tap on the fear to read about it").foregroundColor(.yellow).frame(width: 380, alignment: .leading)
                    List {
                        // Filtered list of fears
                        ForEach(array.filter{$0.hasPrefix(searchText) || searchText == ""}, id:\.self) {
                            searchText in Text(searchText)
                        }
                    }
                 .resignKeyboardOnDragGesture()
                }
            )
        }
    }



    struct FearSearchView_Previews: PreviewProvider {
        static var previews: some View {
                FearSearchView()
                  .environment(\.colorScheme, .dark)
        }
    }

    extension UIApplication {
        func EndEditing(_ force: Bool) {
            self.windows
                .filter{$0.isKeyWindow}
                .first?
                .endEditing(force)
        }
    }

    struct ResignKeyboardOnDragGesture: ViewModifier {
        var gesture = DragGesture().onChanged{_ in
            UIApplication.shared.endEditing(true)
        }
        func body(content: Content) -> some View {
            content.gesture(gesture)
        }
    }

    extension View {
        func resignKeyboardOnDragGesture() -> some View {
            return modifier(ResignKeyboardOnDragGesture())
        }
    }

