//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Edita on 23/02/2021.
//

import SwiftUI

struct ContentView: View {
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var showingScore = false
    @State private var scoreTitle = ""
    @State private var score = 0
    @State private var animationAmount = [0.0, 0.0, 0.0]
    @State private var fade = [1.0, 1.0, 1.0]
    var body: some View {
        ZStack{
            LinearGradient(gradient: Gradient(colors: [.purple, .green]), startPoint: .leading, endPoint: .trailing).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        VStack (spacing: 30){
        VStack{
            Text("Tap the flag of").foregroundColor(.white).font(.largeTitle).fontWeight(.light)
            Text(countries[correctAnswer]).foregroundColor(.white).font(.largeTitle).fontWeight(.black)
        }
        ForEach(0..<3) {
            number in Button(action:{
                self.flagTapped(number)
                
            }) {Image(self.countries[number])
                .renderingMode(.original).clipShape(Rectangle())
                .overlay(Rectangle().stroke(Color.black, lineWidth: 1))
                .shadow(color: .black, radius: 2)
                .opacity(self.fade[number])
                .rotation3DEffect(.degrees(animationAmount[number]), axis: (x: 0, y: 1, z: 0))
            }
        }
            Text("Total Score: \(score)").foregroundColor(.white)
            Spacer()
        }
    }
        .alert(isPresented: $showingScore) {
            Alert(title: Text(scoreTitle), message: Text("Your score now is \(score)"), dismissButton: .default(Text("Continue")){
                self.askQuestion()})
        }
    }
        func flagTapped(_ number: Int) {
            if number == correctAnswer {
                withAnimation(.spring()){
                    self.animationAmount[number] += 360
                }
                var newfade = [0.25, 0.25, 0.25]
                newfade[number] = 1.0
                self.fade = newfade
                scoreTitle = "Correct"
                score += 4
            }
            else {
                scoreTitle = "Wrong! That was the flag of \(countries [number])."
                if score > 0 {
                score -= 2
                }
            }
            showingScore = true
    }
    func askQuestion() {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        fade = [1.0, 1.0, 1.0]
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
