//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Edita on 23/02/2021.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
