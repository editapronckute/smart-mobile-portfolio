//
//  ContentView.swift
//  listnav
//
//  Created by Edita on 12/03/2021.
//

import SwiftUI

struct FearItem: Identifiable {
  let id = UUID()
  let name: String
  let description: String
  let moreinfo: String
}

struct ListOfFearsView: View {
  let fearItem: FearItem
  
  var body: some View {
    ZStack {
      Text(fearItem.name)
        .font(.largeTitle)
    }
  }
}

struct ContentView: View {
    private let listOfFears: [FearItem] = [
       FearItem(
         name: "Acrophobia",
         description: "Acrophobia is an extreme or irrational fear or phobia of heights, especially when one is not particularly high up.",
        moreinfo: "https://en.wikipedia.org/wiki/Acrophobia"),
       FearItem(
         name: "Arachnophobia",
         description: "Arachnophobia is an intense and irrational fear of spiders and other arachnids such as scorpions.",
        moreinfo: "https://en.wikipedia.org/wiki/Arachnophobia"),
        FearItem(
          name: "Philophobia",
          description: "Philophobia is the fear of falling in love. The risk is usually when a person has confronted any emotional turmoil relating to love but also can be a chronic phobia.",
        moreinfo: "https://en.wikipedia.org/wiki/Philophobia_(fear)")
    ]
    
    var body: some View {
        NavigationView {
            List(listOfFears) { fearItem in
                NavigationLink(destination: FearDetailsView(fearItem: fearItem)) {
                HStack {
                  Text(fearItem.name)
                    .font(.headline)
                }.padding(7)
              }
            }
            .navigationBarTitle("Choose a fear")
        }
    }
}

struct FearDetailsView: View {
  let fearItem: FearItem
  
  var body: some View {
    VStack(alignment: .leading) {
      HStack {
        Text(fearItem.name)
          .font(.largeTitle)
          .bold()
        Spacer()
      }
      
      Text(fearItem.description)
        .padding(.top)
        Link("Read more", destination: URL(string: fearItem.moreinfo)!)
      Spacer()
        NavigationLink(destination: MyListView()){
            //take the selected fear and add to a new list in new MyListView()
            Text("Sting it!")
        }
    }
    .padding()
    .navigationBarTitle(Text(fearItem.name), displayMode: .inline)
  }
}

struct MyListView: View {
   // @State var privateList = [""]
    var body: some View {
        VStack(alignment: .leading) {
            // Add to a list each time sting it button is pressed
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
