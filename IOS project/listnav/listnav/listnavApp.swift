//
//  listnavApp.swift
//  listnav
//
//  Created by Edita on 12/03/2021.
//

import SwiftUI

@main
struct listnavApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
