//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Edita on 25/02/2021.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
