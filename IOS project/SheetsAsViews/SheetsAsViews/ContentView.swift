//
//  ContentView.swift
//  SheetsAsViews
//
//  Created by Edita on 09/03/2021.
//

import SwiftUI

struct SecondView: View {
    @Environment(\.presentationMode) var presentationMode
    var msg: String
    
    var body: some View {
            Button("Dismiss") {
                self.presentationMode.wrappedValue.dismiss()
            }
    }
}

struct ContentView: View {
    @State private var showingSheet = false
    var body: some View {
        Button("Show Sheet") {
            self.showingSheet.toggle()
        }
        .sheet(isPresented: $showingSheet) {
            SecondView(msg: "@user")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
