//
//  SheetsAsViewsApp.swift
//  SheetsAsViews
//
//  Created by Edita on 09/03/2021.
//

import SwiftUI

@main
struct SheetsAsViewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
