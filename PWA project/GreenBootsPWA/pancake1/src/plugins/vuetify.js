import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

// export default new Vuetify({
// });

const vuetify = new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#B6D5B6',
                secondary: '#b0bec5',
                accent: '#8c9eff',
                error: '#b71c1c',
            },
        },
    },
})

export default vuetify